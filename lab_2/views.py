from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render
from .models import Note

def index(request):
    notes = Note.objects.all() 
    response = {'Notes': notes}
    return render(request, 'lab2.html', response)
def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")
def json(request):
    notes = Note.objects.all() 
    data = serializers.serialize('json', notes)
    return HttpResponse(data, content_type="application/json")