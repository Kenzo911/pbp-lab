from django.urls import path
from .views import addFriend, index
urlpatterns = [
    path('', index, name='index'),
    path('add', addFriend),
]