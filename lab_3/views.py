from django.shortcuts import redirect, render
from django.http.response import HttpResponse
from .forms import FrienForms
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)
@login_required(login_url='/admin/login/')
def addFriend(request):
    context ={}
    if request.method == 'POST':
        form = FrienForms(request.POST)
        if form.is_valid():
        # save the form data to model
            form.save()
            response = redirect("/lab-3")
            return response
    else:
        form = FrienForms()
    context['form']= form
    return render(request, 'lab3_form.html', context)