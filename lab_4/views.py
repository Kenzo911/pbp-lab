from django.http.response import HttpResponse
from django.core import serializers
from .forms import NoteForm
from django.shortcuts import render, redirect
from lab_2.models import Note

def index(request):
    notes = Note.objects.all() 
    response = {'Notes': notes}
    return render(request, 'lab4_index.html', response)
def addNote(request):
    context ={}
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
        # save the form data to model
            form.save()
            response = redirect("/lab-4")
            return response
    else:
        form = NoteForm()
    context['form']= form
    return render(request, 'lab4_form.html', context)
# Create your views here.
def note_list(request):
    notes = Note.objects.all() 
    response = {'Notes': notes}
    return render(request, 'lab4_note_list.html', response)
