from django.urls import path
from .views import index, addNote, note_list
urlpatterns = [
    path('', index, name='index'),
    path('add-note', addNote),
     path('note-list', note_list ),
]